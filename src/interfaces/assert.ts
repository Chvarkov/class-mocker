export interface IAssert {
    at?: number;
    return?: any;
    call?: (...args: any[]) => any;
    throw?: Error;
}
