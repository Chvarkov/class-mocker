import { IAssert, IType } from './interfaces';
import { fail } from './functions';

type IndexStack = Array<IAssert & {name: string}>;

export class Mock<T> {
    private __callIndex = 1;
    private readonly __asserts: {[key: string]: IAssert} = {};
    private readonly __indexStack: IndexStack = [];
    private readonly __methods: string[] = [];

    constructor(private readonly type: IType<T>) {
    }

    private __call(expect: IAssert, ...args: any[]): any {
        this.__callIndex++;
        if (expect.throw) {
            throw expect.throw;
        }

        if (expect.call) {
            return expect.call(...args);
        }

        return expect.return;
    }

    private __mock(): T & this {
        const mock = {};
        this.__methods.forEach((method) => mock[method] = this.__expectedCallback(method));

        return Object.assign(this, this.type.prototype, mock);
    }

    private readonly __expectedCallback = (name: string) => {
        return (...args: any[]) => {
            const indexExpect = this.__indexStack[this.__callIndex];
            if (indexExpect) {
                if (indexExpect.name !== name) {
                    fail(`Unexpected call ${indexExpect.name}(${args.map(x => JSON.stringify(x)).join(', ')}) at ${this.__callIndex}.`);
                }
                return this.__call(indexExpect, ...args);
            }

            if (this.__asserts[name]) {
                return this.__call(this.__asserts[name], ...args);
            }

            fail(`Unexpected call ${name}(${args.map(x => JSON.stringify(x)).join(', ')}).`);
        };
    };

    expect(method: string, expect: IAssert): T & this {
        if (!this.__methods.includes(method)) {
            this.__methods.push(method);
        }

        if (expect.at) {
            this.__indexStack[expect.at] = {name: method, ...expect};

            return this.__mock();
        }

        this.__asserts[method] = expect;

        return this.__mock();
    }
}
