import { User } from './user';

export class UserRepository {
    private readonly data: Map<number, User> = new Map<number, User>();

    constructor() {
        // Initialize data
        for (const id of [1, 2, 3, 4, 5]) {
            this.data.set(id, new User(id, `User${id}`,  `P@$$W0RD`,`user${id}@test-mail.it`));
        }

    }

    async findById(id: number): Promise<User> {
        await awaitTimeout(1000); // Emulate delay for request to database
        return this.data.get(id);
    }

    async findAll(): Promise<User[]> {
        await awaitTimeout(1000); // Emulate delay for request to database
        return Array.of(...this.data.values())
    }
}

function awaitTimeout(timeout: number): Promise<void> {
    return new Promise<void>(resolve => setTimeout(resolve, timeout));
}
