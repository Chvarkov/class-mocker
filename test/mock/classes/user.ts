export class User {
    id: number;
    username: string;
    email: string;
    password: string;

    constructor(id: number, username: string, password: string, email: string) {
        Object.assign(this, {id, username, email, password});
    }
}
