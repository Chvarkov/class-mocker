import { UserRepository } from './user-repository';
import { User } from './user';
import { Response } from './response';

export class UserController {
    constructor(private readonly userRepository: UserRepository) {
    }

    async find(id: number): Promise<Response<User | string>> {
        try {
            const user = await this.userRepository.findById(id);

            if (!user) {
                return new Response(404, 'Not found.');
            }

            return new Response(200, user);
        } catch (e) {
            return new Response(500, 'Internal server error.');
        }
    }

    async findAll(): Promise<Response<User[] | string>> {
        try {
            return new Response(200, await this.userRepository.findAll());
        } catch (e) {
            return new Response(500, 'Internal server error.');
        }
    }
}
