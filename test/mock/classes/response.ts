export class Response<T> {
    constructor(public readonly statusCode: number, public readonly payload: T) {
    }
}
