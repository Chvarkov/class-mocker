import { Mock } from '../../src';
import { UserRepository } from './classes/user-repository';
import { UserController } from './classes/user-controller';
import { User } from './classes/user';
import { Response } from './classes/response';

describe('Mock', () => {
    it('Use case 1. Test return', async () => {
        const userRepositoryMock = new Mock(UserRepository)
            .expect('findAll', {return: []});

        const userController = new UserController(userRepositoryMock);

        const response = await userController.findAll();

        expect(response).toBeInstanceOf(Response);
        expect(response.statusCode).toBe(200);
        expect(response.payload).toBeInstanceOf(Array);
        expect(response.payload.length).toBe(0);
    });

    it('Use case 2. Test call', async () => {
        const userRepositoryMock = new Mock(UserRepository)
            .expect('findById', {call: (id: number) => {
                expect(id).toBe(2);
                return new User(2, 'ReplacedUser2', 'P@$$W0RD', 'replaced2@test-mail.it');
            }});

        const userController = new UserController(userRepositoryMock);
        const response = await userController.find(2);

        expect(response).toBeInstanceOf(Response);
        expect(response.statusCode).toBe(200);
        expect(response.payload).toBeInstanceOf(User);

        const user = <User>response.payload;
        expect(user.id).toBe(2);
        expect(user.username).toBe('ReplacedUser2');
    });

    it('Use case 3. Test throw', async () => {
        const userRepositoryMock = new Mock(UserRepository)
            .expect('findAll', {throw: new Error('Database is not available')});

        const userController = new UserController(userRepositoryMock);
        const response = await userController.findAll();

        expect(response).toBeInstanceOf(Response);
        expect(response.statusCode).toBe(500);
    });

    it('Use case 4. Test at', async () => {
        const userRepositoryMock = new Mock(UserRepository)
            .expect('findById', {
                at: 1,
                return: new User(4, 'ReplacedUser4', 'P@$$W0RD', 'replaced2@test-mail.it'),
            })
            .expect('findById', {at: 2, return: null});

        const userController = new UserController(userRepositoryMock);

        // At 1
        let response = await userController.find(4);

        expect(response).toBeInstanceOf(Response);
        expect(response.statusCode).toBe(200);
        expect(response.payload).toBeInstanceOf(User);

        const user = <User>response.payload;
        expect(user.id).toBe(4);
        expect(user.username).toBe('ReplacedUser4');

        // At 2
        response = await userController.find(4);

        expect(response).toBeInstanceOf(Response);
        expect(response.statusCode).toBe(404);

        // At 3
        response = await userController.find(4);

        expect(response).toBeInstanceOf(Response);
        expect(response.statusCode).toBe(500);
    });
});
