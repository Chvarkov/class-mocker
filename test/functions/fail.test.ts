import { fail } from '../../src/functions';

describe('Fail function', () => {
    it('Expect throw error', () => {
        expect(() => fail('test text')).toThrow('test text');
    });
});
