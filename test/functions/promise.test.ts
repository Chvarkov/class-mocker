import { promise } from '../../src/functions';

describe('Promise function', () => {
    it('Expect return promise', async () => {
        const value = promise('test');
        expect(value).toBeInstanceOf(Promise);
        expect(await value).toBe('test');
    });
});
